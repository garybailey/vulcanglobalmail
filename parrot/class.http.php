<?php
/**
 * HTTP SOCKET CLASS
 * j0zf 2006.11.29
 * (c) 2006 Copyright ApogeeINVENT, All rights reserved
 *
 * -- ADDED COOKIE SUPPORT - j0zf 2008.2.24
 * -- REUSE BUG FIXES - j0zf 2008.2.24
 * -- ADDED LOOP IN post() AND get() FOR BUGGY DNS CORRECTION -Dwight.2008.10.01
 * -- added ability to set/get/clear user-agent and suppressed cookie header if none are present - j0zf 2010.3.11
 *
 * EASY TO USE EXAMPLES
 *
	// GET - EXAMPLE /////////////////////////////////////////////////////////
	$http = new C_http();
	echo $http->get( 'http://www.j0zf.com/' );

	// SSL POST - EXAMPLE ////////////////////////////////////////////////////
	$http = new C_http();
	$http->set_post_data( "username", "joe" );
	$http->set_post_data( "password", "schmoe" );
	$response = $http->post( 'https://www.apogeeinvent.com/' );

	if( $http->has_errors() ) echo 'ERROR : ' . $http->get_error_number() . ' : ' . $http->get_error_string();
	else echo $response;


	// POSTING XML - EXAMPLE /////////////////////////////////////////////////
	$http = new C_http();
	$http->set_request_header( 'Content-Type', 'text/xml; charset=ISO-8859-1' );
	$http->set_request_body( $xml_string );
	$http->post( 'http://www.somexmlservice.com/xml.php' );
 *
 */

class C_http
{
	var $_url = array();

	var $_timeout = 30;
	var $_http_version = 'HTTP/1.0';
	var $_user_agent = 'PHP-Script (ApogeeInvent)';

	var $_error_number = 0;
	var $_error_string = '';

	var $_cookies = array(); //name => value pair

	var $_request_headers = array(); //hash-table name => value
	var $_post_data = array(); //hash-table name => value
	var $_request_body = '';

	var $_response_headers = array(); //hash-table name => value
	var $_response_body = '';

	// Number of tries before fail when connecting to the server
	// fixed buggy DNS error
	var $num_fsockopen_tries = 5;

	function C_http()
	{

	}

	/** REQUEST ***************************************************************/

	function clear_all()
	{
		$this->_url = array();
		$this->_timeout = 30;
		$this->_http_version = 'HTTP/1.0';

		$this->_error_number = 0;
		$this->_error_string = '';

		$this->_cookies = array(); //name => value pair

		$this->_request_headers = array(); //hash-table name => value
		$this->_post_data = array(); //hash-table name => value
		$this->_request_body = '';

		$this->_response_headers = array(); //hash-table name => value
		$this->_response_body = '';
	}

	function set_url( $url )
	{
		$this->_url = array(
			  'scheme' => 'http'
			, 'host' => ''
			, 'port' => 80
			, 'user' => ''
			, 'pass' => ''
			, 'path' => '/'
			, 'query' => '' // after the question mark ?
			, 'fragment' => '' // after the hashmark #
		);

		$parsed_url = parse_url( $url );

		foreach( $parsed_url as $n => $v )
		{
			$this->_url[ $n ] = $v;
		}

		if( $this->_url['scheme'] == 'https' && !isset( $parsed_url['port'] ) )
		{
			$this->_url['port'] = 443;
		}
	}

	function set_timeout( $val )
	{
		$this->_timeout = $val;
	}

	function set_post_data( $name, $val )
	{
		$this->_post_data[ trim($name) ] = $val;
	}

	function clear_post_data()
	{
		$this->_post_data = array();
	}

	function set_request_body( $str )
	{
		$this->_request_body = $str;
	}

	function set_request_header( $name, $val )
	{
		$name = $this->clean_header_name( $name );
		$this->_request_headers[ $name ] = trim($val);
	}

	function get_request_header( $name, $default_value = NULL )
	{
		$name = $this->clean_header_name( $name );
		return ( isset( $this->_request_headers[ $name ] ) ? $this->_request_headers[ $name ] : $default_value );
	}

	function clear_request_header( $name )
	{
		$name = $this->clean_header_name( $name );
		if( isset($this->_request_headers[ $name ]) ) unset( $this->_request_headers[ $name ] );
	}

	function get_request_headers_string()
	{
		$str = '';
		foreach( $this->_request_headers as $n => $v )$str .= $n . ': ' . $v . "\r\n";
		return $str;
	}

	/** RESPONSE **************************************************************/

	function get_response_body()
	{
		return $this->_response_body;
	}

	function get_response_header( $name, $default_value )
	{
		$name = $this->clean_header_name( $name );
		return ( isset( $this->_response_headers[ $name ] ) ? $this->_response_headers[ $name ] : $default_value );
	}

	function get_error_number()
	{
		return $this->_error_number;
	}

	function get_error_string()
	{
		return $this->_error_string;
	}

	function has_errors()
	{
		$error_no = intval($this->_error_number);
		return ( ( $error_no == 200 || $error_no == 0 ) ? false : true );
	}

	function clear_response()
	{
		$this->_response_headers = array();
		$this->_response_body = '';
		$this->_error_number = 0;
		$this->_error_string = '';
	}

	/** COOKIES ***************************************************************/

	function get_cookie( $n, $default_v = NULL )
	{
	return ( isset( $this->_cookies[$n] ) ? $this->_cookies[$n] : $default_v );
	}

	/**
	* RETURN ALL COOKIES IN ASSOC ARRAY
	*/
	function get_cookies()
	{
		return $this->_cookies;
	}

	function set_cookie( $n, $v )
	{
		$this->_cookies[$n] = $v;
	}

	function set_cookies( $cookies = array() )
	{
		foreach( $cookies as $n => $v )$this->set_cookie( $n, $v );
	}

	function clear_cookie( $n )
	{
		if( isset( $this->_cookies[$n] ) ){ unset( $this->_cookies[$n] ); }
	}

	/**
	* CLEAR ALL COOKIES
	*/
	function clear_cookies()
	{
		$cookies = $this->get_cookies();
		foreach( $cookies as $n => $v )$this->clear_cookie( $n );
	}


	function get_cookies_string()
	{
		$str = '';
		$cookies = $this->get_cookies();
		foreach( $cookies as $n => $v )$str .= urlencode($n) . '=' . urlencode($v) . ";";
		return $str;
	}

	/** UTILITY *******************************************************/

	function clean_header_name( $name )
	{
		return str_replace( ' ', '-', ucwords( str_replace( '-', ' ', trim( $name, ": \t\n\r\0\x0B" ) ) ) );
	}

	/**
	 * BUILD REQUEST BODY
	 * -- if post data exists then urlencode it up....
	 * -- else, leave the "request_body" alone ( for raw xml posts and such )
	 *
	 * bug fix - j0zf 2007.6.2 - clear out request body before appending (for multiple usage of this object)
	 *
	 */
	function build_request_body()
	{
		if( count( $this->_post_data ) > 0 )
		{
			$this->_request_body = '';
			$post_data_i = 0;
			foreach( $this->_post_data as $n => $v )
			{
				if( $post_data_i > 0 )
				{
					$this->_request_body .= '&';
				}

				$this->_request_body .= urlencode( $n ) . '=' . urlencode( $v );
				$post_data_i++;
			}
		}
	}

	function parse_response_header( $str )
	{
		$colon = strpos( $str, ':' );
		$name = '';
		$val = '';

		if( $colon !== false )
		{
			$name = substr( $str, 0, $colon );
			$val = substr( $str, $colon + 1 );
		}
		else
		{
			$name = $str;
		}

		//MAKE SURE WE'RE USING A STANDARDIZED "CASE" NAMING SCHEME... (for hash-table lookup)
		$name = $this->clean_header_name( $name );

		//SNAG THE COOKIES OUT SEPARATELY...
		if( $name == 'Set-Cookie' )$this->parse_set_cookie( $val );

		//SAVE THIS PARTICULAR NAME/VALUE PAIR INTO OUR RESPONSE HEADERS HASH-TABLE
		$this->_response_headers[ $name ] = trim($val);
	}

	function parse_set_cookie( $str )
	{
		$cookie_arr = explode(';',$str);
		if( count($cookie_arr) > 0 )
		{
		$name_value_arr = explode('=',$cookie_arr[0]);
		if( count($name_value_arr) > 0 )
		{
		  $c_name = trim($name_value_arr[0]);
		  $c_value = ( isset( $name_value_arr[1] ) ? $name_value_arr[1] : '' );
		  $this->set_cookie( urldecode($c_name), urldecode($c_value) );
		}
		}
	}

	function full_get_path()
	{
		$str = $this->_url['path'];

		if( $this->_url['query'] != '' )
		{
			$str .= '?' . $this->_url['query'];
		}

		if( $this->_url['fragment'] != '' )
		{
			$str .= '#' . $this->_url['fragment'];
		}

		return $str;
	}

	/** HTTP REQUESTS *********************************************************/

	/**
	 * USE GET METHOD TO _GET REQUEST
	 *
	 */
	function get( $url = '' )
	{
		if( $url != '' )$this->set_url( $url );
		$this->clear_response();

		$errno = 0;
		$errstr = '';

		//try up to 5 times (or # specified by num_fsockopen_tries var) to connect to remote server (fix for buggy DNS resolver) -Dwight 2008-05-12,
		// Taken from AS and applied here ~JospehL 2008.09.10
		$fp = false;
		$i = 1;
		while (!$fp && $i <= $this->num_fsockopen_tries)
		{
			$fp = @fsockopen( ( $this->_url['scheme'] == 'https' ? 'ssl://' . $this->_url['host'] : $this->_url['host'] ), intval( $this->_url['port'] ), $errno, $errstr, $this->_timeout );
			$i++;
		}
		$this->_error_number = $errno;
		$this->_error_string = $errstr;

		if( $fp )
		{
		  //CLEANUP FROM PREVIOUS POST (so http can be reused)
		  $this->clear_post_data();
		  $this->clear_request_header( 'Content-Type' );
			$this->clear_request_header( 'Content-Length' );

			//SEND REQUEST
			$this->set_request_header( 'Host', $this->_url['host'] );

			if($this->get_user_agent()!='') {
				$this->set_request_header( 'User-Agent', $this->get_user_agent() );
			}
			if(count($this->get_cookies()) > 0) {
				$this->set_request_header( 'Cookie', $this->get_cookies_string() );
			}
			$this->set_request_header( 'Connection', 'Close' );
			fwrite( $fp, "GET " . $this->full_get_path() . " " . $this->_http_version . "\r\n" . $this->get_request_headers_string() . "\r\n" );

			//READ RESPONSE HEADERS
			//parse headers till we hit a line containing only a Carriage Return and Line Feed
			$end_of_headers = false;
			while( !feof($fp) && !$end_of_headers )
			{
				$header = fgets( $fp, 4096 );
				
				if( $header == "\r\n" )$end_of_headers = true;
				else $this->parse_response_header( $header );
			}

			//READ RESPONSE BODY
			while( !feof($fp) )$this->_response_body .= fread( $fp, 8192 );

			fclose($fp);
		}

		return $this->_response_body;

	}//~function get( $url = '' )

	/**
	 * USE POST METHOD TO _POST REQUEST
	 * -- use C_http::set_post_data( $name, $val ) to build the "POST" variables to be sent
	 * -- use C_http::set_request_body( $str ) for raw XML posts and other raw posts
	 * 		-- you should also do soemthing like this:
	 * 			 C_http::set_request_header( 'Content-Type', 'text/xml; charset=ISO-8859-1' )
	 */
	function post( $url = '' )
	{
		if( $url != '' )$this->set_url( $url );
		$this->clear_response();
		$this->build_request_body();

		$errno = 0;
		$errstr = '';

		//try up to 5 times (or # specified by num_fsockopen_tries var) to connect to remote server (fix for buggy DNS resolver) -Dwight 2008-05-12,
		// Taken from AS and applied here ~JospehL 2008.09.10
		$fp = false;
		$i = 1;
		while (!$fp && $i <= $this->num_fsockopen_tries)
		{
			$fp = @fsockopen( ( $this->_url['scheme'] == 'https' ? 'ssl://' . $this->_url['host'] : $this->_url['host'] ), intval( $this->_url['port'] ), $errno, $errstr, $this->_timeout );
			$i++;
		}
		$this->_error_number = $errno;
		$this->_error_string = $errstr;

		if( $fp )
		{
			//SEND REQUEST
			$this->set_request_header( 'Host', $this->_url['host'] );
			if($this->get_user_agent()!='') {
				$this->set_request_header( 'User-Agent', $this->get_user_agent() );
			}
			if(count($this->get_cookies()) > 0) {
				$this->set_request_header( 'Cookie', $this->get_cookies_string() );
			}
			if( $this->get_request_header('Content-Type') === NULL )
			{
				//ONLY SET CONTENT TYPE IF THE HEADER HASN'T ALREADY BEEN MANUALLY SPECIFIED..
				//example types -- text/xml; charset=ISO-8859-1   OR   application/x-www-form-urlencoded    ETC...
				$this->set_request_header( 'Content-Type', 'application/x-www-form-urlencoded' );
			}
			$this->set_request_header( 'Content-Length', strlen( $this->_request_body ) );
			$this->set_request_header( 'Connection', 'Close' );

			fwrite( $fp, "POST " . $this->full_get_path() . " " . $this->_http_version . "\r\n" . $this->get_request_headers_string() . "\r\n" . $this->_request_body );

			//READ RESPONSE HEADERS
			//parse headers till we hit a line containing only a Carriage Return and Line Feed
			$end_of_headers = false;
			while( !feof($fp) && !$end_of_headers )
			{
				$header = fgets( $fp, 4096 );

				if( $header == "\r\n" )$end_of_headers = true;
				else $this->parse_response_header( $header );
		  }

			//READ RESPONSE BODY
			while( !feof($fp) ) $this->_response_body .= @fread( $fp, 8192 );

			fclose($fp);
		}

		return $this->_response_body;
	}//~function post( $url = '' )

	/**
	 * GET/SET/CLEAR USER-AGENT
	 * - J0ZF 2010.3.11
	 */
	function set_user_agent($v){ $this->_user_agent = $v; }
	function get_user_agent(){ return $this->_user_agent; }
	function clear_user_agent(){ $this->set_user_agent(''); }

}//~class C_http

?>