<?php

/*
API call to allow Peter to add a random email address to our database
Need some basic authorisation key, just to avoid external attacks
We are not on the same IP address any more, so cannot use that method - but a simple auth key in the post should cover it
*/

//First step is the basic auth - using hardcoded passcode
$checkaccess = md5("monetize".date("Ymd"));
// echo $checkaccess." : {$_REQUEST['access']}\r\n";
if ($_REQUEST['access'] !== $checkaccess) die ("Unauthorised posting");
# load all required files
$configfile = "../config/config.php";
include ($configfile);
require_once '../admin/init.php';

echo "Files loaded\r\n";

//Validate our email address:
$_REQUEST['email'] = stripslashes(trim($_REQUEST['email']));
$tmpEmail=filter_var($_REQUEST['email'], FILTER_SANITIZE_EMAIL);
if ( !filter_var($tmpEmail, FILTER_VALIDATE_EMAIL) ) { die ("Not a valid email address {$tmpEmail}"); }

$_REQUEST['GUID'] = addslashes(trim($_REQUEST['GUID']));


//Now open up our database:
/* 
	$database_host
	$database_name
	$database_user
	$database_password
*/		
					
	$db = mysql_connect($database_host, $database_user, $database_password);
	if (!mysql_select_db($database_name,$db)) { die("could not select database"); }
		
//Now let's start on some queries
//Check to see if we have a valid list number specified
$sql = "select * from phplist_list where id='{$_REQUEST['listid']}'";
echo "{$sql}\r\n";
if (!$result = mysql_query($sql)) die ("Could not check list ids");
if (mysql_num_rows($result)<1) die ("Invalid list ID specified");

//Check to see if this email address already exists
$sql = "select id,uniqid from phplist_user_user where email like '{$_REQUEST['email']}'";
echo $sql."\r\n";
if (!$result = mysql_query($sql)) die ("Could not verify existing record");
if (mysql_num_rows($result)>0) {	//This is an existing record - make sure that this lead is on the specified list
	$temp = mysql_fetch_assoc($result);
	
	$userid = $temp['id'];
	
	//Check to see if this user is already on this list
	$sql = "select * from phplist_listuser where `userid`='{$userid}' AND `listid`='{$_REQUEST['listid']}'";
	echo "{$sql}\r\n";
	if (!$result = mysql_query($sql)) die ("Could not verify against list table");
	if (mysql_num_rows($result)<1) {
		//Add into list
		$sql = "insert into phplist_listuser (`userid`,`listid`,`entered`) VALUES ('{$userid}','{$_REQUEST['listid']}','".date("Y-m-d H:i:s")."')";
		echo "{$sql}\r\n";
		if (!$result = mysql_query($sql)) die ("Could not add user to list {$_REQUEST['listid']}");
		$msg = "Added to list {$_REQUEST['listid']}";
	} else {
		$msg = "Already on list {$_REQUEST['listid']}";
	}
	
} else {

	//Create our new records
	$uniqid = md5(uniqid(mt_rand()));
	$sql = "insert into phplist_user_user "
			." (`email`,`uniqid`,`foreignkey`,`confirmed`,`htmlemail`,`entered`) "
			." VALUES "
			." ('{$_REQUEST['email']}','{$uniqid}','{$_REQUEST['GUID']}','1','1','".date("Y-m-d H:i:s")."')";
	echo "{$sql}\r\n";
	if (!$result = mysql_query($sql)) die ("Could not add new record");
	
	//Pull back the id number to add this lead to the right list
	$sql = "select id,uniqid from phplist_user_user where uniqid like '{$uniqid}'";
	echo "{$sql}\r\n";
	if (!$result = mysql_query($sql)) die ("Could not pull back ID #");
	$temp = mysql_fetch_assoc($result);
	// print_r($temp);
	/*
	foreach ($temp as $key => $value) {
		echo "{$key} => {$value}\r\n";
	}
	*/
	$userid = $temp['id'];
	
	//Add into list
	$sql = "insert into phplist_listuser (`userid`,`listid`,`entered`) VALUES ('{$userid}','{$_REQUEST['listid']}','".date("Y-m-d H:i:s")."')";
	echo "{$sql}\r\n";
	if (!$result = mysql_query($sql)) die ("Could not add user to list {$_REQUEST['listid']}");
	
	//History message:
	$msg = "Auto import";
}

	//Finally log a history update for this email address
	$sql = "insert into phplist_user_user_history "
		." (`userid`,`ip`,`date`,`summary`,`detail`) "
		." VALUES "
		." ('{$userid}','{$_SERVER['REMOTE_ADDR']}','"
		.date('Y-m-d H:i:s')
		."','{$msg}','{$msg}')";
	echo "{$sql}\r\n";
	if (!$result = mysql_query	($sql)) die ("Could not update user history records");


echo "Success";

?>