<?php

/*
API call to allow Peter to remove an existing email from a specified list
Need some basic authorisation key, just to avoid external attacks
We are not on the same IP address any more, so cannot use that method - but a simple auth key in the post should cover it
*/

//First step is the basic auth - using hardcoded passcode
$checkaccess = md5("monetize".date("Ymd"));
// echo $checkaccess." : {$_REQUEST['access']}\r\n";
if ($_REQUEST['access'] !== $checkaccess) die ("Unauthorised posting");
# load all required files
$configfile = "../config/config.php";
include ($configfile);
require_once '../admin/init.php';

echo "Files loaded\r\n";

//Validate our email address:
$_REQUEST['email'] = stripslashes(trim($_REQUEST['email']));
$tmpEmail=filter_var($_REQUEST['email'], FILTER_SANITIZE_EMAIL);
if ( !filter_var($tmpEmail, FILTER_VALIDATE_EMAIL) ) { die ("Not a valid email address {$tmpEmail}"); }

$_REQUEST['GUID'] = addslashes(trim($_REQUEST['GUID']));


//Now open up our database:
/* 
	$database_host
	$database_name
	$database_user
	$database_password
*/		
					
	$db = mysql_connect($database_host, $database_user, $database_password);
	if (!mysql_select_db($database_name,$db)) { die("could not select database"); }
		
//Now let's start on some queries
//Check to see if we have a valid list number specified
$sql = "select * from phplist_list where id='{$_REQUEST['listid']}'";
echo "{$sql}\r\n";
if (!$result = mysql_query($sql)) die ("Could not check list ids");
if (mysql_num_rows($result)<1) die ("Invalid list ID specified");

//Check to see if this email address already exists
$sql = "select id,uniqid from phplist_user_user where email like '{$_REQUEST['email']}'";
echo $sql."\r\n";
if (!$result = mysql_query($sql)) die ("Could not verify existing record");
if (mysql_num_rows($result)>0) {	//This is an existing record - make sure that this lead is on the specified list
	$temp = mysql_fetch_assoc($result);
	
	$userid = $temp['id'];
	
	//Check to see if this user is already on this list
	$sql = "select * from phplist_listuser where `userid`='{$userid}' AND `listid`='{$_REQUEST['listid']}'";
	echo "{$sql}\r\n";
	if (!$result = mysql_query($sql)) die ("Could not verify against list table");
	if (mysql_num_rows($result)<1) {
		//If it's not there - nothing to do
		$msg = "Email not on list ID # {$_REQUEST['listid']}";
	} else {
		$sql = "delete from phplist_listuser where `userid`='{$userid}' AND `listid`='{$_REQUEST['listid']}'";
		echo "{$sql}\r\n";
		if (!$result = mysql_query($sql)) die ("Could not remove from list table");
		$msg = "Removed from list # {$_REQUEST['listid']}";
	}
	
} else {
	die ("This lead does not exist");
}

	//Finally log a history update for this email address
	$sql = "insert into phplist_user_user_history "
		." (`userid`,`ip`,`date`,`summary`,`detail`) "
		." VALUES "
		." ('{$userid}','{$_SERVER['REMOTE_ADDR']}','"
		.date('Y-m-d H:i:s')
		."','{$msg}','{$msg}')";
	echo "{$sql}\r\n";
	if (!$result = mysql_query	($sql)) die ("Could not update user history records");


echo "Success";

?>