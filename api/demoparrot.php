<?php

/*  Postal Parrot demo script
	Sends a quick email to my email address only
	*/

//Pull in our emailer class
include_once dirname(__FILE__).'/../parrot/class.http.php';
	
//Some specific API settings for our servers
$api_url = 'https://www.powerlinemail.com/postal_server.php';
$api_user = 'vulcan';
$api_hash = 'd41d8cd98f00b204e9800998ecf8427e';
	
//Sample data for our test email only
$mylist = "vulcan".microtime();
$tolist = array("mybetterway@gmail.com","cris.alcasid@gmail.com");
$from = "gary@myhmbiz.com";
$subject = "This is the parrot speaking";
$text = "This is the text portion";
$html = "<h1>And this is the html portion</h1>";	
	

foreach ($tolist as $to) {
//Using Joel's code base:
 //Must opt prospect in.
	$api_requests[] = array(
			'cmd' => 'opt_in'
			, 'data' => array(
					'email' => $to,
					'list_name' => $mylist
					)
				);
   $http = new C_http();
		$http->set_post_data( 'api_user', $api_user );
		$http->set_post_data( 'api_hash', $api_hash );
		$http->set_post_data( 'api_requests', serialize($api_requests) );
		//$http_response = $http->post($api_url);
										
		
		$api_requests=array();
		$api_requests[] = array(
		  'cmd' => 'create_creative_and_send',
		  'data' => array(
				'email_addr' => $to,
				  'from' => $from,
				'subject' => $subject,
				  'text' => $text,
				'html' => $html
		  )
		);

		$http = new C_http();
		$http->set_post_data( 'api_user', $api_user );
		$http->set_post_data( 'api_hash', $api_hash );
		$http->set_post_data( 'api_requests', serialize($api_requests) );
		//print_r($api_requests);

		$http_response = $http->post($api_url);
		$recipients = @unserialize($http_response);
		print_r($recipients);

		//Parse out the success / failure
		$success = $recipients[0]['status'];
		print_r($success."\r\n");
		$result1 = explode('/',$success);
		$result2 = explode(':',$result1[0]);
		print_r("Result is : {$to} - ".trim($result2[1])."\r\n");
}

//Now call our class to do the work
/*
//Opt everyone in
$http = new C_http();
$http->set_post_data( 'api_user', $api_user );
$http->set_post_data( 'api_hash', $api_hash );
$http->set_post_data( 'api_requests', serialize($api_optin) );
*/
/*
//Send the emails
$http = new C_http();
$http->set_post_data( 'api_user', $api_user );
$http->set_post_data( 'api_hash', $api_hash );
$http->set_post_data( 'api_requests', serialize($api_requests) );

$http_response = $http->post($api_url );
$api_responses = @unserialize($http_response);

print_r($api_responses);
*/

echo "Done";