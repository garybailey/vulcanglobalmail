<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0040)http://vulcanglobalmail.com/lists/admin/ -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
<link rev="made" href="mailto:info%40phplist.com">
<link rel="home" href="http://www.phplist.com/" title="phplist homepage">
<link rel="license" href="http://www.gnu.org/copyleft/gpl.html" title="GNU General Public License">
<meta name="Author" content="Michiel Dethmers - http://www.phplist.com">
<meta name="Copyright" content="Michiel Dethmers, phpList Ltd - http://phplist.com">
<meta name="Powered-By" content="phplist version 3.0.8">

<link rel="SHORTCUT ICON" id="favicon" href="http://vulcanglobalmail.com/lists/admin/images/phplist.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- initial styles and JS from basic application -->

<link rel="stylesheet" href="http://vulcanglobalmail.com/lists/admin/css/reset.css">

<link rel="stylesheet" href="http://vulcanglobalmail.com/lists/admin/css/app.css">
<link rel="stylesheet" href="http://vulcanglobalmail.com/lists/admin/css/menu.css">

<!-- now override the above with the styles and JS from the UI theme -->
<link rel="stylesheet" href="http://vulcanglobalmail.com/lists/admin/ui/dressprow/css/base.css">
<link rel="stylesheet" href="http://vulcanglobalmail.com/lists/admin/ui/dressprow/css/layout.css">
<link rel="stylesheet" href="http://vulcanglobalmail.com/lists/admin/ui/dressprow/css/skeleton.css">
<link rel="stylesheet" href="http://vulcanglobalmail.com/lists/admin/ui/dressprow/css/style.css">
<link rel="stylesheet" href="http://vulcanglobalmail.com/lists/admin/ui/dressprow/css/gray.css">

<!-- Style for rtl language <link rel="stylesheet" href="ui/dressprow/css/style_rtl.css" /> -->

<title>phpList :: Vulcan :: File Upload</title>
<link rel="icon" href="http://vulcanglobalmail.com/lists/admin/images/phplist.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="http://vulcanglobalmail.com/lists/admin/images/phplist-touch-icon.png">
<link rel="apple-touch-icon-precomposed" href="http://vulcanglobalmail.com/lists/admin/images/phplist-touch-icon.png">
</head>
<body class="home">
<div id="dialog"></div><div id="hiddendiv"></div>

<div id="container" class="container_24 container">
	<div id="header" class="grid_24 sixteen columns">
		<div id="logo"><a href="http://www.phplist.com/" target="_blank"><img src="http://vulcanglobalmail.com/lists/admin/ui/dressprow/images/branding.png" alt="phpList, email newsletter manager, logo" title="phpList, the world&#39;s most popular Open Source newsletter manager"></a></div>
		<div id="logged"></div>
	</div>
	<!-- end .grid_24 -->
	<div class="clear"></div>
	<div id="navigation" class="grid_24 sixteen columns">
<div id="main-menu" class=" fourteen columns alpha">
<div id="menuTop"></div>
<div class="two columns omega">
<span id="menu-button" style="display: none;"><span>Menu</span></span>
</div>
</div>
<div id="globalhelp">
</div>
<div class="clear"></div>
<div id="content" class="grid_20 twelve columns">
<div id="wrapp">
<div id="progressbar"></div>
<!-- content start here -->
<h4 class="pagetitle">Upload your lead files here:</h4>
<!-- ENDOF .header -->
<div class="content">

<?php
//Setup some constants
$filemax = 5;	//Max number of files to be uploaded at one time

if ($_POST['setfiles']=='setfiles') {
	/*
	echo "<pre>";
	print_r($_POST);
	print_r($_FILES);
	echo "</pre>";
	*/
	
	//Run through our list of files:
	$i=0;
	while ($i<$filemax) {
		$i++;
		if ($_FILES["file{$i}"]['error'] == 4) continue;
		//Parse filename - looking for CSV or TXT files
		if (substr($_FILES["file{$i}"]['name'],-4) !== '.txt' && substr($_FILES["file{$i}"]['name'],-4) !== '.csv') {
			echo substr($_FILES["file{$i}"]['name'],-4)." Not a valid file type - ".$_FILES["file{$i}"]['name']."<br>";
			continue;
		}
	  //Get the temp file path
	  $tmpFilePath = $_FILES["file{$i}"]['tmp_name'];
	
	  //Make sure we have a filepath
	  if ($tmpFilePath != ""){
		//Setup our new file path
		$newFilePath = "./uploadFiles/" . $_FILES["file{$i}"]['name'];
	
		//Upload the file into the temp dir
		if(move_uploaded_file($tmpFilePath, $newFilePath)) {
			echo $_FILES["file{$i}"]['name']." uploaded for processing<br>";
		} else {
			echo $_FILES["file{$i}"]['name']." FAILED<br>";
		}
	} else {
			echo "No temp file path defined for ".$_FILES["file{$i}"]['name']."<br>";
	}
  }
}
?>

<div  style='text-align:center;'>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
<input type='hidden' name='setfiles' id='setfiles' value='setfiles' />
    <table cellpadding="0" cellspacing="0" border="0" width="60%" class="listing "><tbody>
    <?php
	$lists = "";
	$i = 0;
	while ($i<$filemax) {
		$i++;
		echo "<tr><td>File #{$i}:</td>"
			."<td><input name='file{$i}' id='file{$i}' type='file'></td>"
			."<td><input name='list{$i}' id='list{$i}' type='select'>"
			.$lists
			."</select></td>"
			."</tr>"; 
	}
	?>
    <tr><td colspan='2' align='center'><input type="submit" /></td></tr>
    </tbody></table>
</form>
</div>
  </div><!-- ENDOF .content -->
<div class="footer">
      
	<div class="clear"></div>

</div> <!-- end #container -->

<div id="footer">
    <div class="container">
    </div>
</div>
<!-- mz 07-07-2014--></body></html>