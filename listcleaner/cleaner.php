<?php

/* email validation script
	start with basic proving of email validation principles
	then move to pulling line by line from a file, validating and adding to our database as a new lead
	*/
	
$emails = array ('mybetterway@gmail.com','thisisnot@anemail.co.uk','whattheheck');

foreach ($emails as $key => $value) {	
	$domain = array_pop(explode('@', $value));
	if (!filter_var($value, FILTER_VALIDATE_EMAIL)) continue;
	if (!checkdnsrr($domain, 'MX')) continue;
	echo $value."\r\n";
}

?>